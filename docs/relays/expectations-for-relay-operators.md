## Expectations for Relay Operators

The **Expectations for Relay Operators** document strives to be a clear and comprehensive guide for the relay operator community.
This is an effort to keep the Tor community and the network safe, healthy, and sustainable.

Version 2.0 \
Last updated: 2024-12-28

## 1. Keep users safe

* Don't look at, or modify, network traffic.
* Don't reveal user or destination IP addresses, or the timing or volume of connections. For instance, don't create a publicly available webpage showing bandwidth history or any statistics about the machine (e.g. CPU/RAM usage) with an aggregation window smaller than a day as this information might be used in surprising ways to attack users.
* By default (e.g. unless you are debugging a specific thing), log at loglevel "notice", and leave `SafeLogging` on.
* Running a bridge and a public relay using the same IP address is discouraged as censored users won't be able to connect to the network.
* If you run more than one relay, set the `MyFamily` option for each of them, to instruct clients to avoid using more than one in a circuit, and to help the [Network Health team](https://gitlab.torproject.org/tpo/network-health/team) know which relays are really yours.
* Don't modify your Tor process to examine internal state.
In particular, don't record onion addresses.
* Don't proxy your relay exit traffic through a VPN or other proxies.
It can introduce more surface area (more places get a chance to see the traffic). Second, it seems to be correlated with people trying to do shady things with their traffic, and third, having that extra hop can impact the network performance.
* If you want to measure or study the Tor network, do it safely: write up your plan first and get feedback from the [Tor Research Safety Board](https://research.torproject.org/safetyboard/).

## 2. Maintain good system operational security (opsec)

* Always follow the official [Tor relay guide](https://community.torproject.org/relay/) documentation. Do not rely on advice from random websites or projects that are not affiliated with or endorsed by the Tor Project.
* Run a version of Tor that is supported.
You can see supported versions on the [Network Team wiki page](https://gitlab.torproject.org/tpo/core/team/-/wikis/NetworkTeam/CoreTorReleases). Relays and bridges running Tor versions that are end-of-life (EOL), unsupported versions, or with known security vulnerabilities we will follow the [Relay EOL Policy](https://gitlab.torproject.org/tpo/network-health/team/-/wikis/Relay-EOL-policy) and remove from the network.
* Watch your Tor logs for warnings and try to address them.
* Make sure to keep the rest of your system software up to date too.
* Try not to let adversaries get your relay secret keys, but if they do, make sure to contact the [bad relays team](https://community.torproject.org/relay/community-resources/bad-relays/), so we can block those keys from the network.

## 3. Make sure your relay isn't broken

* Tor relays need 10000+ file descriptors, so be sure your Tor package or startup script is raising your `ulimit -n`.
* Don't firewall outgoing connections. Tor relays need to be able to reach all other Tor relays, including new ones that are self-testing their reachability.
* Exit relays need a working DNS resolver. Also, don't use a [DNS resolver that censors its answers](https://gitlab.torproject.org/tpo/network-health/team/-/wikis/Criteria-for-rejecting-bad-relays).
* If there's a port or address block that your exit relay can't reach, be sure to reject it in your `ExitPolicy`, so clients can know to use a different exit.

## 4. Sustainability

* Don't try to run an exit relay at a place where you plan to just discard it and move on if the ISP complains to you.
Running an exit relay involves advocacy to your ISP, to help them understand how Tor works and why it's important.
* If somebody tries to get you to backdoor or tap your relay, don't do it.
Find a lawyer and fight it, and if you can't fight it, shut the relay down instead.

## 5. Keeping the Tor network diverse and decentralized

* Relay operators should contribute, promote, and maintain a decentralized Tor network. To prevent overconcentration and centralization, operators are expected to not run more than 20% of the total exit capacity or more than 10% of consensus weight. While it is acknowledged that certain circumstances, such as a DDoS scenario where some relays perform better than others, may temporarily result in exceeding this threshold unintentionally, such occurrences should not be the result of deliberate actions by the operator.
* Intentional efforts by an operator to control a disproportionate share of the Tor network are unacceptable and undermine the network's decentralization and trustworthiness.
* Running a relay is better than not running any, but, promote geographic and network diversity to reduce the risk of centralization in [a few Internet providers](https://metrics.torproject.org/bubbles.html#as).
* Relay operators are encouraged to review and contribute to the [good/bad ISPs](https://community.torproject.org/relay/community-resources/good-bad-isps/) page.

## 6. Be a part of our community

* Remember that running a relay is an act of transparency (even though being a Tor *user* is an act of privacy), because the way to strengthen trust in relays is by having a stronger community.
* To maintain transparency and strengthen collaboration within the Tor community, operators running significant amount of relays are encouraged to keep in touch with the Tor Project.
* Make an effort to meet other relay operators and developers in your area and at conferences and meetups.
* Be sure to set your `ContactInfo` to a working email address in case we need to reach you. If you're concerned about spam, you can create a specific email address for your relay.
* Running a relay is great because it helps to make Tor users safer.
Please make sure you're not undermining Tor or Tor user safety in your other activities.
For example, if your other hobby is developing surveillance tools for a shady company, or making people fear or hate privacy tools, please do not also run a Tor relay.
* If you can't run a relay, consider donating to one of the [relay associations](https://community.torproject.org/relay/community-resources/relay-associations/) to strengthen the Tor community and support their efforts.
* Read our social contract and the other [community documents](https://community.torproject.org/policies).

If there is a question about whether you or your relay are following the expectations laid out here, these are the documents we will be using to guide our choices.

## 7. Don't be a jerk, be awesome instead

* Relay operators are expected to follow and promote the Tor Project [Code of Conduct](https://community.torproject.org/policies/code_of_conduct/). 
* Be respectful, inclusive, and supportive. Experienced relay operators are encouraged to support new operators by sharing knowledge and providing guidance on the tor-relays mailing list and other community channels.
* Relay operators can get a Tor T-shirt for contributing to the Tor project. Please check the [criteria and if you're eligible](https://community.torproject.org/relay/community-resources/swag/).

## Contributing to this document

If you have feedback for improving this document, you can open a new ticket on the [Community Policies repository](https://gitlab.torproject.org/tpo/community/policies).
The repository is also available on [Anon-Ticket](https://anonticket.onionize.space/), which does not require a GitLab account.
