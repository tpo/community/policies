# Membership Policy

These guidelines are intended to define Core Contributor roles and
responsibilities. Anyone who uses, supports, or contributes to Tor can be a
member of the Tor community, but being a Core Contributor is a recognition of
demonstrated, continuous work on Tor plus interactions with other Core
Contributors. Core Contributors get special access to Tor resources, are
involved with the day-to-day work of building Tor, and are considered
ambassadors of Tor in the broader community. We hope that these guidelines will
clarify the privileges and responsibilities of being a Core Contributor so that
we may all focus our time on working well together and contributing positively
to Tor.

This membership concept is distinct from whether people are employees or
contractors for the non-profit organization "The Tor Project Inc": Tor the
non-profit organization is different from Tor Core Contributors. The Tor
Project Inc and the Core Contributors support the same goals with an
overlapping, but not necessarily identical, set of responsibilities.

This document describes rules and procedures. We as a community frown on gaming
these rules and procedures by following them to the letter while subverting
their intent. Please don't do that.

## What is a "Core Contributor"?

We call a community member a Core Contributor when they...

 * Have worked continuously on Tor for at least the last six months.
 * Enjoy the respect and support of other Tor Core Contributors.
 * Have gone through the process outlined in this document.

The phrase "work on Tor" includes but is not limited to: advocacy, support,
design, code, community building, project management, bug triaging,
documentation, etc.

The Tor Community is made up of all kinds of people -- researchers, relay
operators, advocates, developers, and users. Some of those people work on Tor
as part of their day job, while others contribute a few lines of code or a bug
report here and there. All contributions -- big and small -- are what make Tor
possible, and we're grateful for such a vibrant and dedicated community.

## How can I become a Core Contributor?

Members of the wider Tor community may become a Core Contributor by doing the
following...

1. Find an Advocate who is an existing Core Contributor.
2. You and your Advocate collaborate to answer the following:
    * What name or alias would you like to be called?
    * How have you been involved in Tor these last six months?
    * What is your OpenPGP key?
    * What email should be subscribed to our internal lists?
3. Your Advocate initiates an internal discussion with the other Core
Contributors. Two weeks of discussion and consensus-building will then take
place. Any Core Contributor has the ability to "block" if they have very
serious concerns about a potential new contributor. A "block" is not a veto
in the traditional sense; it means that the concerns are so serious that
they would sooner leave the project than allow for the new contributor to
join. Since blocking is a very serious measure, we hope that the
overwhelming majority of decisions about whether or not to invite someone
to become a Core Contributor will be decided by consensus-building alone. 

If consensus is reached after two weeks, you become a Core Contributor. If
consensus fails, that result will be shared with you and it is suggested you
wait at least six months before trying again, and work with your Advocate to
understand why your membership was rejected. If your application is blocked,
you may be prohibited from reapplying.

## What are the benefits of being a Core Contributor?

Core Contributorship has the following privileges...

 * One vote, whenever [voting](voting.md) takes place.
 * Subscription to the tor-internal@ and related mailing lists.
 * Access to the #tor-internal and related IRC channels.
 * Listing on the ["core people"](https://www.torproject.org/about/people) webpage.
 * An @torproject.org forwarding address.
 * Technical and computing resources as required and available. Note
   that access to authenticated resources requires your OpenPGP key to
   be signed by at least one other Core Contributor.

## What responsibilities are there to being a Core Contributor?

Core Contributors are expected to...

 * Agree and abide by the Tor [Social Contract](social_contract.md).
 * Bring conflicts or concerns with other Core Contributors to the
   [Community Council](community_council.md).
 * Continue actively working on Tor and interacting with other Core
   Contributors.

We emphasize the *community* nature of being a Core Contributor: it is not
enough to just advance Tor's goals; you also have to participate in the
community. Building trust by interacting as human beings, in person when
possible, is the best way to maintain a strong and robust Tor Project.

## How can my status as a Core Contributor end?

There are three methods...

### 1. Voluntary

Core Contributors are free to step out of the project at any
time. We kindly ask that it's done carefully, transitioning any
responsibilities. To retire, please send an email either publicly
on tor-project@ or privately on tor-internal@.

### 2. Inactivity

Volunteers naturally both come and go, and this is fine. If you
haven't been actively involved with Tor in the preceding six months
the Account Manager will contact you to ask if you'd like to start
actively contributing again. If not, your membership as a Core
Contributor may be suspended.

If there is disagreement about whether a level of involvement meets
this criteria or not, the Community Council will be asked to decide.

Contributors who have their status discontinued for this reason are of
course welcome and encouraged to remain in the wider Tor community,
and they may go through the process of becoming a Core Contributor
again if they wish. Returning contributors don't necessarily need
to accumulate a fresh six months of work on Tor before reapplying; 
other Core Contributors can opt to waive that requirement during the
application period.

### 3. Involuntary

Unfortunately in some circumstances it might become necessary to
"banish" someone from being a Core Contributor. This can only
happen with a decision from the Community Council. We consider
this a last resort and would rather resolve any issues through e.g.
community-based approaches.

When there is a serious question about whether someone's Core
Contributor status should be revoked, they will temporarily lose
access to the internal mailing list(s) where their membership status
is being discussed. If this happens, the Account Manager will...

  * inform the person of the matter
  * remove the person from the lists while things are under discussion
  * give the person a clear way to relay matters to the list during
    their not-being-on-the-list

Involuntary termination will result in revocation of the privileges
listed above. Mail forwarding for involuntarily terminations will
be determined on a case-by-case basis.

## Who is the Tor Project Account Manager?

The Tor Project Account Manager, aka Membership Secretary, helps set up and
maintain Tor accounts. Their tasks are to ensure that the processes that we
agreed upon are being respected and to follow the advice of the Community
Council about closing any account. The Account Manager acts in consultation
with longstanding members of the Tor community. Every quarter the Account
Manager, with the help of longstanding community members, should review the
list of Core Contributors and look for Contributors without any visible
activity since the last review.

The Account Manager is an elected position that follows the same procedure as
the [Vote](voting.md) Secretary.

## Are the privileges like email forwarding addresses restricted to Tor Core Contributors?

No, some of those things are necessary for other work, for example, employees
need an @torproject.org address as soon as they are hired, and many members of
our community get invited to the Tor Meeting. 
