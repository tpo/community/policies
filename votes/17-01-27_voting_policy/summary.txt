=====================================================================
Summary
=====================================================================

Topic: Voting Procedure
Date taken: 17/1/13 - 17/1/27
Vote count: 27 (of 81 eligible voters, 33%)

Secretary: Karsten Loesing
Proposer: Damian Johnson

Topic put to a vote were...

  Question 1: Voting Proposal

    We didn't get any alternate proposals this is a simple up-or-down
    vote so please choose one of the following...

    a. I approve of the attached proposal becoming our initial
       voting policy.

    b. I reject the attached proposal.

  Question 2: Quorum

    Please choose your ordered preference of the following options...

    a. Votes require a 1/10 quorum of eligible voters.
    b. Votes require a quorum consisting of a square root of eligible voters.
    c. No quorum requirement.

  Question 3: Ratification Delay

    Please choose your ordered preference of the following options...

    a. Proposals take effect when the vote is concluded.

    b. Proposals take effect one week after the vote is concluded
       if there are no objections. If there are objections the
       proposal is suspended for two additional weeks for the
       results to be audited.

    c. Proposals take effect three weeks after the vote is concluded.

Results were...

  * Proposal passes. 96% approved of its adoption. We'll continue
    to run with this as our initial voting policy.

  * We'll require a quorum consisting of a square root of eligible
    voters. This was picked 19 to 6 (76%).

  * We'll delay results for one week with an extension if auditing is
    called for. This was picked 20 to 3 (87%).

=====================================================================
Votes
=====================================================================

Alison
Allen Gunn
Arthur D. Edelstein
Damian Johnson
David Fifield
David Goulet
isis
iwakeh
Jens Kubieziel
Linus Nordberg
Lunar
Mark Smith
meejah
Moritz Bartl
Nick Mathewson
Nima Fatemi
Pepijn Le Heux
Peter Palfrader
Philipp Winter
Roger Dingledine
Sebastian Hahn
Silvia [Hiro]
Steven Murdoch
teor
Tom Ritter
Wendy Seltzer
Yawning Angel

996; a; a, b, c; b, c, a
1726; <abstain>; <abstain>; <abstain>
2905; a; a, b, c; b, a, c
4256; a; b, a, c; b, a, c
4868; a; b, c, a; <abstain>
7446; a; b, a, <none>, c; b, a, c
7966; a; b; a
8027; a; b, a, c; b, c, a
8242; a; a, b; b, a
8882; a; b, a, c; b, c, a
14134; b; b; b
14712; a; b, c, a; a, b, c
15399; a; a; b
16890; a; b, c, a; b, c, <none>
19252; a; a, b, <none>, c; b, c, <none>, a
19617; <abstain>; <abstain>; <abstain>
20939; a; b, a, c; b, a, c
21000; a; b, a, c; b, c, a
21322; a; b; b, a, c
22844; a; a, b, c; b, c, a
23594; a, b; b, a, c; b, c, a
25399; a, b; b, a, <none>, c; <abstain>
28568; a; c, b, a; a, b, c
28922; a; b, a, c; b, c, <none>, a
28945; a; b, a, c; b, c, a
30454; a; b; b, a, c
31610; a; b, a, <none>; b, a, c

=====================================================================
Question 1: Voting Proposal
=====================================================================

Approve: 24 (96%)
Reject: 1 (4%)

---------------------------------------------------------------------
Result:
---------------------------------------------------------------------

Motion passes. We'll continue to run with this for our
initial voting policy.

=====================================================================
Question 2: Quorum, Round 1
=====================================================================

a. Votes require a 1/10 quorum of eligible voters.

996, 2905, 8242, 15399, 19252, 22844


b. Votes require a quorum consisting of a square root of eligible voters.

4256, 4868, 7446, 7966, 8027, 8882, 14134, 14712, 16890, 20939, 21000,
21322, 23594, 25399, 28922, 28945, 30454, 31610


c. No quorum requirement.

28568

---------------------------------------------------------------------
Result:
---------------------------------------------------------------------

Option c (No quorum requirement) eliminated, 28568's vote goes to 'b'.

=====================================================================
Question 2: Quorum, Round 2
=====================================================================

a. Votes require a 1/10 quorum of eligible voters.

996, 2905, 8242, 15399, 19252, 22844


b. Votes require a quorum consisting of a square root of eligible voters.

4256, 4868, 7446, 7966, 8027, 8882, 14134, 14712, 16890, 20939, 21000,
21322, 23594, 25399, 28922, 28945, 30454, 31610, 28568

---------------------------------------------------------------------
Result:
---------------------------------------------------------------------

Option b (square root quorum) is picked 19 to 6 (76%).

=====================================================================
Question 3: Ratification Delay
=====================================================================

a. Proposals take effect when the vote is concluded.

7966, 14712, 28568

b. Proposals take effect one week after the vote is concluded
   if there are no objections. If there are objections the
   proposal is suspended for two additional weeks for the
   results to be audited.

996, 2905, 4256, 7446, 8027, 8242, 8882, 14134, 15399, 16890,
19252, 20939, 21000, 21322, 22844, 23594, 28922, 28945, 30454,
31610

c. Proposals take effect three weeks after the vote is concluded.

[none]

---------------------------------------------------------------------
Result:
---------------------------------------------------------------------

Option c (one week delay with extention if audit is needed) is picked
20 to 3 (87%).

